\begin{figure}[h]
	\centering
	\includegraphics[width=15cm]{images/overall}
	\caption{The Workflow of the Approach}
	\label{fig:workflow}
\end{figure}

We construct our approach in a series of stages as depicted in the figure \ref{fig:workflow}. Our approach is iterative and constructs a new generation of test cases at every iteration with the aim of revealing performance bugs. \par

In our approach we use Neuroevolution \cite{neuroevo} to iteratively generate testcases that can reveal performance bugs. Neuroevolution uses genetic algorithm combined with neural networks to maximize a fitness function that measures the performance in a task. In our case this performance is the time that is taken by the generated test case.  

Neuroevolution uses genetic algorithm to refine the weights of the neural network for reinforcement learning as opposed to other ways of tuning the networks weights through a feedback mechanism.

This technique is suitable for our approach because unlike other neural networks which require a large corpus of input-output examples,  this can modify its behavior as long as the performance of the network can be evaluated over time. It is also highly general making it suitable to generate test cases for a large number of different subjects like browsers and media players. There are also numerous ways in which the performance of the network can be evolved and updated for example evolving the topology of the network itself for better results \cite{neat}. \par 
Our approach takes in two inputs, namely, the input database and an initial test suite. The input database is used to construct more complex test cases. These are usually application specific, for example in the case of Firefox, the database would consist of URL and its subsequent DOM element mappings. An important property for this input database has to be it's ability to preserve the dependency between elements. This is required because the DOM element of one URL would not exists in another URL. We also provide an initial starting test suite to our approach. This would consist of test cases specific to the AUT under study for example for Firefox it would be Selenium test cases.

\section{Workflow}

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/sampletestcase}
	\caption{Sample test case}
	\label{fig:sample}
\end{figure}

The approach consists of a series of stages as depicted by the figure \ref{fig:workflow}. The process begins with an input AUT specific test suite. For example in the case of Firefox we have a suite of selenium test cases as seen in figure \ref{fig:sample}.

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/xmldatabase}
	\caption{Input database for Web Browsers}
	\label{fig:databrow}
\end{figure}

At the Initialization phase we provide this test suite and an input database of elements. The input database of elements is AUT specific and needs to maintain the dependency between the elements. For example the database for Firefox as an AUT could be an XML file like the one depicted the figure \ref{fig:databrow}.

Here we can see that each test case has URL elements which has several child nodes called \textit{elem}. Each elem has different properties and is different for different parent URL elements. This denotes the relationship that exists between parent and child nodes and can be used to create test cases with more interactions. \par
The Initialization phase aims to create a AUT independent representation of the AUT specific test case. Each test case in the input test suite is mapped to an Abstract test case which is a tree representation of the actions in the test case. Each abstract test case is modeled into a Genotype which is the encoding of information required by Neuroevolution \cite{neuroevo}. In our case each genotype contains the encoding for the underlying Neural Network. We discuss more about the genotype when we talk about the Initialization phase. The Initialization phase is not part of the iterations and is run only once in the approach. It is a set up phase that prepares the artifacts for the next stages. \par
After the initialization stage, the execution and comparison stage executes the test cases in the test suite on both the AUT and the Oracle. It flags those test cases that take statistically significantly more time for the AUT than for the Oracle.  The output of this stage is a set of flagged execution traces.\par
The next stage the Profiling and Feature Extraction stage takes as input these flagged test cases, then profiles them to extract certain features that can be used to predict program behavior. These features are known as seminal features and they are usually program features such as loop trip counts and function counts. These features are the output of this stage and pass on to the next stage the New Test Suite Generation stage\par
The Test generation stage genetically evolves the initial test suite into new mutated tests using a genetic algorithm. This cost becomes the fitness value that we aim to maximize during the course of the approach in order to unearth more performance bugs. This stage also creates a new generation of test cases which forms the new test suite. This new generation comprises of both the AUT specific test cases and their abstract test cases. Their corresponding abstract test cases and their underlying genotype encoding are also created in this stage. \par
For the sake of convenience we refer to iterative section of the work flow (i.e. from comparison stage, to profiling to test generation/execution and to performance threshold checking) as the \textit{loop}. After the New Test Suite Generation stage has finished execution a new test suite is created and the loop is ready to execute again however before the loop begins anew, the Report Overlapped Code Region phase executes. This stage is a reporting stage and is responsible for creating reports for the code regions extracted in the process along with the test cases that took longer to execute on the AUT than on the Oracle. The developer can now analyze these code sequences to narrow down the locations of the performance bug. In the next subsections, we discuss each stage in the approach in greater detail.

\subsection{Initialization}

The Initialization stage is a configuration/setup stage. This stage is important because it provides a layer of abstraction which strips away the AUT specific details and keeps the structure of the test case. This is required since we run the approach for a variety of applications. \par
We do this via creating an abstract representation of each AUT specific test cases provided as initial input to the approach. This abstract test case is a tree representation of the underlying structure of the test case and models it's dependencies. Each abstract test case comprises of edges and nodes. Each edge represents an action for example a \textit{Mouse Click} and each node represents an element. Each of the these edges have a cost assigned to them which in the beginning is random. Later on this cost is modified based on the seminal features extracted about which we discuss in the later sections of the approach. This information allows us to convert the abstract test case back into an AUT specific format when it required to be executed. \par
We can see in figure \ref{fig:TCFirefox} and \ref{fig:TCVLC}, sample abstracted test cases for Firefox and VLC.


\begin{figure}[h]
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=5cm]{images/AbstractTCFirefox}
		\caption{Abstract Testcase for Firefox}
		\label{fig:TCFirefox}	
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=5cm]{images/AbstractTCVLC}
		\caption{Abstract Testcase for VLC}
		\label{fig:TCVLC}	
	\end{subfigure}
	\label{fig:abstract}
	\caption{Abstract Test Cases}
		
\end{figure}


After the abstract test cases have been generated, the approach generates genotype representation from the abstracted test case. Genotype represents the encoding of information. In our case it  represents how each abstract test case is mapped to a neural network representation which form the basic component, aka as the genome for genetic algorithms. For this work the Genotype is an object that contains a series of slots with each slot containing the following information
\begin{itemize}
	\item  Type $ \in \{ \ Sensor, \ Hidden , \ Output  \} $
	\item Weight	
\end{itemize} 
Where $Sensor$ is the input node and the output for the genome is obtained from the $Output$ node. In this work the topology of the network does not evolve so the $Sensor$ nodes maps to the $Hidden$ nodes which maps to the $Output$ Node. However the structure is flexible and can be easily altered to allow evolving topologies.
\par
The mapping of the abstract test case to the genotype is as follows. The $Sensor$ nodes are the input slots. The extracted Seminal feature is applied to these slots. For this thesis we consider one seminal feature so there is one slot but the method can be easily customized to contain more seminal feature slots. Next there are two nodes which map to hidden layer and lastly there is one output layer node depicted by the figure as node 5 which maps to the edges of the given abstract test case. In the beginning each slot is assigned with a random weight. \par

\begin{figure}
	\centering
	\includegraphics[width=15cm]{images/GenotypevsPhenotype}
	\caption{Genotype vs Phenotype}
	\label{fig:genvsphen}
\end{figure}

Each genotype also maps into a phenotype which is a network representation for the genome. In other words Phenotype is the representation of the information contained in the corresponding genome. There can be many ways to map the genotype to the phenotype for our purposes we use a direct encoding to map from the genotype to the phenotype which means every slot in the genotype maps directly to a node in the phenotype. We can see this in Figure \ref{fig:genvsphen}. Initially the weights of the genotype is initialized with random values which would get fine-tuned after the test case is executed in the next iteration.\par

It is to be noted that this stage is executed once in the approach to setup the abstract testcases and their genotypes. In the next iterations this stage is not executed. \par

At the end of this stage we have created a suite of abstract test cases and their subsequent genotype encoding. Once this information has been created we can proceed to the next stages. 

\subsection{Execution and Comparison}

\begin{figure}[h]
	\centering
	\includegraphics[width=15cm]{images/Comparison}
	\caption{The Comparison Module}
	\label{fig:comparison}
\end{figure}

The approach begins with the Comparison module. The comparison module executes the testcases in the test suite on both the AUT and the Oracle to obtain their execution times. 

Those execution traces for which the AUT took significantly longer than the Oracle are flagged as executions of interest and their execution traces are filtered for the next stages. 

As an example we take a look at Figure \ref{fig:comparison}. We consider Firefox as our AUT and Chrome as our oracle. The Comparison module executes the test cases in the test suite on both the AUT and the Oracle. The execution time for the test cases are recorded. For those Testcases in which Firefox took 10 times longer than the time required by Oracle is flagged as execution of interest. The execution traces of Firefox for these TestCases are filtered and passed on to the next stages for data extraction and analysis. 

\subsection{Profiling and Feature Extraction}


\begin{figure}[h]
	\centering
	\includegraphics[width=15cm]{images/ProfilingandFeature}
	\caption{The Profiling and Feature extraction module}
	\label{fig:profile}
\end{figure}

The execution traces obtained from the previous stage are analyzed by a profiler in this stage to extract features from them that might enable effective prediction of execution times. 

Extracting  features from real world software can be complex. In order to tackle this, we use \textit{Seminal Features} \cite{stat}. Seminal Features are program behaviours that have strong statistical correlation with other program behaviours and are revealed early on in the program execution. Program behaviours can range from function call frequencies to loop counts to values which are set directly from the input. As an example of seminal features consider two loops, \textit{X} and \textit{Y} in the program and assume that the program is executed with \textit{n} different tests from a test suite. Loop \textit{X} has the trip counts : $\{x_1,x_2 \dots x_n \}$. Loop \textit{Y} has the trip counts : $\{y_1,y_2 \dots y_n \}$. It can be shown that two loops can have a linear relationship that can be expressed as  $ X=aY + c$ , where a and c are weights. Thus if we know the value of loop \textit{Y} earlier in program execution, we can predict the behavior of \textit{X}. This makes \textit{Y} a seminal feature. A lot of program behaviors are candidates for seminal features. However, in this thesis we consider function call frequencies,loop counts and sequence of functions as  prospective seminal feature candidates. \cite{stat} \par

Once the seminal feature set are extracted they form the vector $v$. The vector $v$ can be used in a mapping function $ B=f(v) $; where \textit{B} is the target behavior to be predicted. Constructing the mapping function \textit{f} is a learning problem. The target behavior \textit{B} in this case is the core behavior that can be used to construct the next test input. \textit{B} in our case is the cost of the edge described by the abstract test case.\par

We  can plug the seminal feature set $v$, extracted from the previous step into a classifier $ B=f(v) $, to learn the function \textit{f}. The target of this step is to generate input centric data models through learning that are capable of predicting the behavior of the overall test case based on program features. \par

Here we use the neural network represented by the genome of the test case to learn the function  \textit{f}.\par

As we can see in the figure \ref{fig:profile}, we take the execution traces obtained from the previous module and pass it to the Feature Extraction Module. The Feature Extraction module extracts the seminal feature. The Feature Extraction Module is usually specific to the feature that we want to use as the seminal feature for the current iteration. For example the implementation for a Feature Extraction Module to extract loop counts as the Seminal Feature is different than a Feature Extraction Module that aims to extract Function counts as the seminal Feature. \par

After the Feature Extraction module has finished execution we come up with a set of seminal features that we can use to construct a new test case. These seminal features are passed on to the next stage which is the new test suite generation module.


\subsection{New Test Suite Generation Module}

\begin{figure}[h]
	\centering
	\includegraphics[width=15cm]{images/newtest}
	\caption{The New Test Generation Module}
	\label{fig:newtest}
\end{figure}

The new test suite generation module receives as input the seminal features extracted from the previous stage. It also receives as input the suite of abstract test cases generated from the previous modules. 

These seminal features are used as an input to the Genetic Algorithm which would construct a new generation of test cases for the next iteration of the loop.

Once the seminal features have been extracted we use them to recalculate the cost of the abstract test case maintained by each genome. The seminal feature is input to the genome representation of the test case and it's output becomes the cost of the edge in the abstract test case. \par 

Each abstract test case maps to it's genome representation which is neural network encoding and produces the following output. \par
 
Output of every node :  $\sum_{i = 1}^{i = n} I_{i}\ \times \ w_{i} $

Where $n$ is the total number of input to the node. $w$ is the weight associated with the input. The outputs of every node in cascaded till it reaches the output node of the genotype. This output is a number ranging from 0 to 1 and becomes the cost of the edge in the abstract test case.\par 

This cost is used as the fitness function parameter for the selection procedure. 

Before we can proceed to next stages of our approach we have to define two methods which are necessary for all genetic algorithms.


\begin{itemize}
	\item The Selection function
	\item The Crossover function 
\end{itemize}

\subsubsection{The Selection Function}

The Selection function defines the algorithm by which the fittest of the generation are chosen to pass over their genetic material over to the next generation. The selection function always does this via a fitness function that it wants to maximize or minimize. For neuroevolution this fitness function is usually a direct feedback for the performance of the network. In our case we chose as fitness function to maximize cost of the abstract tree maintained by each genome. We choose at each iteration genomes that has the maximum cost. 
This allows us to build over user test cases that have taken longer for the AUT than for the oracle in hope that there may be another performance problem related to this test case. For example if the testcase : $\{URL \ A \ \rightarrow Click \rightarrow Dom element \ B\}$ takes more time, it is worthwhile to investigate and build more test cases related to the test case under consideration. 

\subsubsection{The Crossover Function}

Another function required by any genetic algorithm is the crossover function which defines how the material from the chosen parent is to be passed on to the next generation. In our case we have to pass on the genetic material of both the abstract test case as well as the genome it is mapped to. 

There are two scenarios that we come across during this procedure.

\begin{itemize}
	\item The Chosen Genomes have same root elements
		
	If the chosen genome have same root elements but with different costs. Then the child element with the higher cost is chosen as the new abstract test case. The weights of the underlying genome is swapped between the chosen parents. This can be seen in figure \ref{fig:diffparents} and figure \ref{fig:genotype}
	
	\begin{figure}[h]
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\includegraphics[width=7cm]{images/diffparents}
			\caption{Scenario where there are different parents}
			\label{fig:diffparents}
		\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{images/crossover}
		\caption{Crossover for the genotype}
		\label{fig:genotype}
	\end{subfigure}
	\caption{Crossover function}
	\label{fig:totalcrossover}
	
	\end{figure}
	
	 
	\item The Genome is mutated
	 Occasionally based on the previously decided mutation rate of the approach, a totally new abstract test case is created along with its genome representation. This is done so that each generation of test cases can have variety amongst themselves  as seen in figure \ref{fig:mutate}
	 
	 	\begin{figure}[h]
	 	\centering
	 	\includegraphics[width=9cm]{images/mutate}
	 	\caption{Scenario where there is mutation}
	 	\label{fig:mutate}
	 \end{figure}
	
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=5cm]{images/NewTestCase}
	\caption{A new test case}
	\label{fig:newtest2}
\end{figure}


After the new test case has been created we append to the created test case by adding another child node to it's abstract test case and another output node to it's genome representation. This new node that is chosen has to be a dependent element of the leaf node of the parent. This allows us to the maintain the dependency of the existing test case. This choice is random and allows us to grow our test cases so that it can reveal more performance bugs. The valuation of this new test case would be evaluated in the next iteration when it is run as depicted by Figure \ref{fig:newtest2}


This module provides two outputs, one the new test suite and second is the execution traces considered in this stage which is reported to the developer who can analyze the execution traces that took significantly longer in order to pin point the region where the performance bug is hidden.  \par

At this stage AUT specific test cases are created from the new suite of abstract test cases and are ready to be executed by the next iteration of our approach.

\begin{figure}[h]
	\centering
	\includegraphics[width=15cm]{images/report}
	\caption{Report Overlapped Code Regions}
	\label{fig:report}
\end{figure}


\section{Report Overlapped Code Regions}

At the end of each iteration the developer is presented with the test cases that took significantly longer to execute on the AUT as well as the execution traces. With this he/she can take a closer look at the code regions which might hide the performance bug. \ref{fig:report}



\section{Condition for Loop Termination}

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/loopterminate}
	\caption{Condition for Loop Termination}
	\label{fig:terminate}
\end{figure}

The approach iterates through these modules at each generation constructing more test cases that impact performance. The loop terminates when the time difference between two successive runs is less than a certain threshold as depicted by figure \ref{fig:terminate}.

\section{Example}
In order to illustrate the process from start to finish  we walk through our approach with a simple example.

We take Firefox as our AUT and Chrome as our oracle. We start with a new Test Suite consisting of a set of selenium test cases as input to the approach which we call the set \textit{input}. \par


\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/sampletestcase}
	\caption{Sample test case}
	\label{fig:samplets}
\end{figure}

Each input test case is of the form depicted by the figure \ref{fig:samplets} where \textit{x} belongs to the set URL :  $\{ "https://s3.amazonaws.com/mozilla-games/ZenGarden/\\EpicZenGarden.html?playback\&novsync\&cpuprofiler\&fakegl",  "www.python.org",\\ "www.google.com" \}$ \\ Each $x$ in this set is an URL.

At the initial stage the approach creates an abstract test case from each test case in the test suite. 

i.e. $\forall y \ \in \ input \ \exists  a $ 
 where \textit{a} is the Abstract test case. As seen previously the abstract test case is of the form as depicted by figure \ref{fig:abstractTC} where X is every element of the set URL. \par


\begin{figure}[h]
	\centering
	\includegraphics[width=5cm]{images/SampleAbstract}
	\caption{Sample Abstract Test Case}
	\label{fig:abstractTC}
\end{figure}

Each abstract test case is mapped to it's genotype representation as depicted by figure \ref{fig:genotype}. The corresponding phenotype is also depicted by the figure \ref{fig:pheotype}.Each $w$ depicted in the genotype and the phenotype representation represent the weights for the test case. At this stage they have been initialized with random values.


\begin{figure}[h]
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{images/genotype}
		\caption{Genotype representation from the abstract test case}
		\label{fig:genotype}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{images/phenotype}
		\caption{Phenotype representation for the abstract test case}
		\label{fig:pheotype}
	\end{subfigure}
	\caption{Genotype and Phenotype representation for the abstract test case}
	\label{fig:genoandpheno}
	
\end{figure}

After executing the test cases representation in the comparison module we notice that the test case with the URL = $\{"https://s3.amazonaws.com/mozilla-games/ZenGarden/epicZenGarden.html?playback\&novsync\&cpuprofiler\&fakegl"\} $ perform significantly slower for Firefox than Chrome. 

This execution sequence is profiled in the Profile and Feature Extraction stage. As an sample seminal feature we use \textit{loop counts}. From here we extract the seminal feature value as $\{val\}$ which is applied to out genotype representation to get the value at the output node $\{out\}$. This $\{out\}$ value becomes the cost of the edge in the abstract test case. \par 

After this the approach moves on the New test suite generation phase. Here it uses the Selection algorithm as discussed before the select the parents with maximum cost in their abstract Test Case. \par

In the case of our example test case with the parent with the URL $\{"https://s3.amazonaws.com/mozilla-games/ZenGarden/epicZenGarden.html?\\playback\&novsync\&cpuprofiler\&fakegl"\} $ is chosen. As in the crossover function discussed in the previously, we chose a child element of the URL from the input database to create a new child node, the corresponding abstracts test case, the genotype and the mapped phenotype are altered as well. This is done by creating a new output node in the genotype and phenotype representation as depicted by the figure \ref{fig:newgenoandpheno}. The newly created nodes have a random weight that will get fine tuned in the subsequent iterations. \par 

The AUT specific test cases are also created from the newly created abstract test cases. For our example a new generation of Selenium test cases are created which would get executed in the next iteration of our approach. \par


\begin{figure}[h]
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{images/newGenotype}
		\caption{Altered Genotype when a new test case is created}
		\label{fig:newgenotype}
	\end{subfigure}
	\hfil
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{images/newphenotype}
		\caption{Altered phenotype when a new test case is created.}
		\label{fig:newpheotype}
	\end{subfigure}
	\caption{Altered Genotype and Phenotype of a newly created test case}
	\label{fig:newgenoandpheno}	
\end{figure}

An example selenium test case generated for the abstract test case described above, would be as seen in  figure \ref{fig:seleniumTC} :

\begin{figure}
	\centering
	\includegraphics[width=15cm]{images/SeleniumTestCase}
	\caption{Selenium Test case}
	\label{fig:seleniumTC}
\end{figure}

This process is continued till an entire new test suite is created and the approach is ready to iterate through the stages again.

Before the approach begins the loop again it goes into the Report Overlapped Code sections stage where we see that the following code sections are executed for all iterations of the test case with the URL that took relatively longer to execute for the AUT than for the Oracle. We see that in function \textit{ProcessExecutableMemory.cpp} the following lines of code  are being executed as shown in figure \ref{fig:error}. After closer inspection it is revealed that the following lines \textit{1} contains a performance bug number 1345205 \cite{Bug1345205} which was caused by an excessive buffer size. The fix can be seen in the figure \ref{fig:noterror}. 

\begin{figure}[h]
\centering
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{images/bufferror}
		\caption{Code segment that contains the performance bug}
		\label{fig:error}
	\end{subfigure}
	~\\
	~\\~\\
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{images/noterror}
		\caption{Code segment after fix}
		\label{fig:noterror}
	\end{subfigure}
\label{fig:errornoerror}
\caption{Code segments before and after fix}
\end{figure}

After the Report Overlapped Code region phase the loop begins again till the terminating condition as specified above is not satisfied.