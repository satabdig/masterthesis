
Performance is an essential attribute of every software system. Many software systems cannot be used or are abandoned because of performance problems. I would like to highlight how important software performance can be with two examples:

\begin{itemize}
	\item Health Insurance:
	The federal government health insurance portal showed slow responses and outages soon after it was launched in October last year resulting in user distress and fallen expectations. The website crashed often due to the heavy traffic generated from large number of users trying to register and use the portal. Reportedly, the website was never adequately tested for performance and load bearing capacity.\cite{govhealth}
	
	\item Distributed Order Management System:
	 Massachusetts revamped its website to meet the demands of the federal Affordable Care Act. The new website was supposed to inform people whether they qualified for a subsidized plan, help them calculate the cost of coverage, and enable them to compare plans and to enrol. The website did not work as planned right from the beginning, it has serious performance issues causing people to file paper applications and even caused several to be without coverage for months forcing the state to enrol them in temporary insurance plans through the state Medicaid program.\cite{healthinsurance}
\end{itemize} 

The above mentioned examples illustrate the inability of a software to meet
it's objectives because of performance issues. These examples demonstrate the importance of software performance, and illustrates that performance is a key attribute and desirable property of any software. Software performance can be especially crucial for embedded systems \cite{impPerf}, any fault in which can result in loss of life and property.

\section{Motivation and Problem Statement}

Software performance is an important metric that can either make or break a software system in today's competitive market. Software performance is measured by how fast and efficient a software can complete it's tasks \cite{software}.\par 
A performance problem or bug usually involves high resource consumption or slow response time. For this thesis, we consider slow response time as a criterion for performance bugs. \par
Not only do performance bugs frustrate and annoy users, they can also cause substantial damage to reputation and finance. 
In spite of being a very important class of bugs present in real world software, little work has been done on them as compared to security and functional bugs \cite{qualitative}.\par

Performance bugs are difficult to diagnose, they require more effort to fix and it is important to study and analyze them, in order to provide more insights, techniques and tools for their diagnosis and repair. Performance bugs are different and require special care \cite{secvsperf}. As the work by Shahed Zaman et al. \cite{secvsperf} stated performance bugs are fixed 2.5 times slower than other bugs including security bugs.  In addition, the authors found that performance bugs also require more experienced developers to solve and fix them. The work also states that performance bugs fixes are 1.6 times more complex than other bugs and require 2.6 times more changes in files than for other bugs. This thesis proposes a dynamic approach to help with performance bug detection by generating a test suite targeting performance bugs specifically, and also isolating code locations where of code where performance bugs might be hidden. \par

\section{Challenges}
However, detecting performance bugs is often a difficult task. Detecting and Fixing performance bugs are often given low priority as compared to other bugs like security and functional bugs this means they are considered to be less severe and take a long time to fix. Most compilers and other state-of-the-art bug detection tools are not built to detect performance bugs. Detecting performance bugs becomes difficult because of the following reasons:
\begin{itemize}
    \item{Difficult to Identify} \\ First, performance bugs are difficult to identify because they have "no formal definition". One way to identify performance bugs is by comparing the performance of the test software with the performance of another similar software. The software, with which the performance is compared to, is referred to as the oracle. For example consider the Bug:1247843 \cite{favicon} which reported unnatural load times for some websites in Firefox \cite{firefox}. This behaviour was reported against performance observed on other web browsers like Chrome \cite{Chrome} for the same workload. Hence, for this bug, the Chrome web browser was used as the oracle. 
    \item{Lack of Defined Coverage} \\
    Another reason that makes the exploration of performance bug difficult is the absence of a well defined coverage criterion for Performance Bugs. Random tests are often the only way to expose performance bugs which often prove to be ineffective and inefficient\cite{randomtesting}. Systematic exploration of performance bugs has not been investigated adequately, making developers ill-equipped to deal with them.\par
    \item{Valid Test Generation}\\
    Typically, tests are generated specifically to test functional bugs. As performance bugs are different from functional bugs they cannot be revealed by these tests. Our approach suggests a way to generate valid test cases through reinforcement learning that we discuss in later stages of this thesis. 
\end{itemize}



\section{Goal of the Thesis}

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{images/PerfBug}
	\caption{Example of a Performance Bug}
	\label{fig:perf}
\end{figure}

In this thesis, we propose an approach to address the challenges related to detecting performance bugs. Our idea is based on the insight that executions of code blocks that appear more frequently in known performance bugs are likely to reveal unknown performance bugs. It can also reveal crucial clues on how to resolve such an issue. For instance, the favicon bug  \cite{favicon} was revealed by exploring the program trace for Firefox. The bug leads to an increase in response time of the browser. This is caused by individual POST requests being dispatched whenever the parser encountered a "favicon" load request. \par
For this thesis we define performance bug as defined in the work by Guoliang Jin \cite{perfbugs} - as \textit{code sequences, which upon slight modification gives significant boost to system performance}.  Based on this definition we relate performance drops to areas of code. This allows us to use correlation between code features to construct a platform independent approach to generating tests which can reveal performance bugs.\par
For instance, a performance bug present in Apache server, reveals that the variable \textit{APR\_TYPE} on being replaced with \textit{APR\_DEFAULT} caused a performance boost of almost two times with respect to time. However detecting this change is not easy as described above and the bug in the example took 2 years to fix.  \par
Our approach tackles the difficulty of identifying performance bugs accurately by defining oracles. Oracles are usually predefined software that perform a similar task as that of the AUT or they can also be expected response time periods as defined by the developer. For this thesis we consider Oracles as predefined software. The Oracle is meant to provide clear benchmarks in performance which can be used to compare with the performance of the AUT. The performance of the Application Under Test (AUT) is compared with that of the oracle. Investigating the tests in which the AUT has performed badly as compared with the oracle can reveal the occurrence of performance bugs. This gives a clear criterion as to which tests we should investigate to reveal performance bugs in the AUT. \par
Another difficulty of creating tests that can reveal performance bugs is to classify appropriate code regions that are likely to reveal performance bugs. Our approach relies on the correlation that exists in program behaviours like loops and function calls with the observed performance drop \cite{stat}. We use machine learning techniques to find such correlation and we leverage this correlation to generate more test cases that reveal drops in performance.\par
In summary, we have designed a platform independent, dynamic framework that can be used to expose performance bottlenecks in a wide variety of real software.




\section{Approach Overview}

Our approach aims to generate a test suite that can reveal that can reveal performance bugs. This approach can be broken down into several stages, each of which perform a particular function. 
The process aims to extract relevant features from the code which can predict system behaviour. These features are used to learn which test can reveal performance bugs. The process is iterative and builds more complex test cases based on it's previous learning \cite{stat}. As a result it creates a test suite containing test which are tailor made for performance bug detection. Each iteration of the approach creates new test suite of performance impacting tests based on the knowledge gained in previous iterations. The approach continues till there is no significant time difference between two iterations of the approach. Thus this process becomes a platform independent, general method that used learning to generate performance impacting tests for all software. 


\section{Contributions}

This work provides the following main contributions:

\begin{enumerate}
	\item Creation of a platform independent, dynamic framework for generation of test cases for performance bug detection. 
	\item This work also provides a corpus of 33 real world performance bugs existing in popular software like Firefox, VLC.
	\item Finally, the approach suggested has been able to detect two new real world performance bugs, one for Firefox and one for Libreoffice. 
\end{enumerate}

\section{Organization}
In Chapter 2, we talk about the background work which are closely associated with our work. In Chapter 3, we describe the methodology of our approach in details. We discuss each stage of the approach suggested by the work in a more fine grained level. In Chapter 4, we discuss the specifics of the implementation. In Chapter 5, we evaluate the approach through a series of experiments. This chapter deals with the details of these experiments and their corresponding results. We also mention the limitations of the work and factors which might be threat to it's validity. In Chapter 6, we summarize the work done in this thesis and derive conclusions. In Chapter 7, we discuss the future work that can be done using the approach and also how the approach itself can be refined.