
We aim to answer the following research questions through this evaluation

\begin{itemize}
	\item \textbf{Research Question 1}: How effective is our approach in generating new test cases and detecting new performance bugs?
	\item \textbf{Research Question 2}: What effect does seminal feature have on our approach?
	\item \textbf{Research Question 3}: How do we account for non-determinism in our approach?
	\item \textbf{Research Question 4}: How sensitive is our approach to varying rates of mutation?
	
\end{itemize}

\section{Set Up}

The subjects that we have selected for evaluating our approach are real world open source software like browsers and media players. \par

Our approach is generic and can work with most software. The only requirement is that source code for the AUT must be provided so that a profiler can run on it.  \par

The Oracle for every AUT must be predefined or can be provided as a set of desired timings. In this thesis we have defined an Oracle for every subject that we have used. We can see the list of the chosen subjects along with their Oracles in table \ref{tab:AllSub}.


\begin{table}[h]
	\centering
	\caption{List of the subjects chosen for evaluation}
	\label{tab:AllSub}
	\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|l|l|l|l|l|l|l|}
		\hline
		Program Type   & AUT Name        & Oracle & Number of bugs extracted & Extraction period & Number of fixed perf bugs & Year of Establishment    \\ \hline
		Web browser    & Mozilla Firefox & Chrome & 13 & 10 months &  3504  & 1998       \\ \hline
		Media Player   & VLC             & Smplayer   & 6 & 5 months & 1465  &  2001  \\ \hline
		Word Processor & Libre Office    & Microsoft Word & 7 & 2 weeks & 3664 & 2010\\ \hline
		Text Editor    & Kate            & Gvim   & 7 & 2 months & 2924 & 2001\\ \hline     
	\end{tabular}
	}
\end{table}



We attempt to answer our research questions via investigating performance bugs existing in the our test subjects. We mine these bugs from the error reporting repositories for these subjects. First we introduce the Bugs chosen for evaluation. A complete table of each bugs mentioned here along with the link to their repositories is available in the appendix.



\subsubsection{Performance Bugs for Test Subject : Firefox}

We test our approach against the following Bugs mined from Bugzilla, Mozilla as seen in the table \ref{tab:firefoxbugs}. Mozilla has an extensive community and a well documented repository. The errors are reported in Bugzilla which goes through a series of stages before it is fixed. Initially a bug is reported as unconfirmed. Depending on the priority, a developer either confirms the bugs and allocates a developer to it or marks it as \textit{Invalid}, \textit{Works for me}, \textit{Duplicate} or \textit{Wont fix}. Bugzilla was created in $1998$ \cite{BugzillAge} when Mozilla was a nascent project. Mozilla always uses a \textit{perf} tag to indicate performance bugs. Mozilla uses Mercurial \cite{mercurial} as it's main repository. Since the repository is very extensive and performance bugs takes years to fix, it took $10$ months to mine all the bugs. We chose those bugs that have a resolution date of 2016 and after as there has not been significant changes to the architecture of Mozilla and we can build them easily. We searched with the keyword \textbf{perf} to look for performance bugs. All these bugs have been fixed in the current release.

\begin{table}[h]
	\centering
	\caption{Bugs for Mozilla}
	\label{tab:firefoxbugs}
	\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|p{2cm}|p{13cm}|l|l| l |}
		\hline
		Bug Number  & Bug Description & Report Date & Resolution Date & LOC affected                                                                                                                                            \\ \hline
		Bug 1171966 & Degraded animation performance especially in case of mouseover in Firefox.& 2015.6.5 & 2016.11.07  & 4                                                                                           \\ \hline
		Bug 1329815 & WebGl demo is slow and laggy in Firefox. & 2013.2.15 & 2017.03.20   & 62                                                                                                                                   \\ \hline
		Bug 1345205 & Zen gardens WASM demo runs slowly in Firefox. & 2017.03.07 & 2017.06.16  & 2                                                                                                                          \\ \hline
		Bug 1329988 & Webgl demo  is slow and laggy in Firefox. & 2017.01.10 & 2017.04.14   & 25                                                                                                                            \\ \hline
		Bug 1209697 & Firefox rendering is extremely slow when Flexbox row is combined with absolutely positioned overlays&2015.09.29 & 2017.04.14 & 1                                                        \\ \hline
		Bug 1360441 & Copying a large amount of data in sites like JSFiddle is slow for Firefox& 2017.04.27 & 2017.05.19 &  10                                                                                    \\ \hline
		Bug 1359341  & Firefox gives slow page notifications on https://pastebin.com/EpK0Z41P.& 2017.4.25 & 2017.5.25 & 6                                                                                      \\ \hline
		Bug 1294161 & The implementation for handling ==,was faulty.,This causes intense lag in certain webpages like “Washington Posts” & 2016.8.10 & 2017.1.4 & 18                                          \\ \hline
		Bug 1329901 & FlagAllOperandsAsHavingRemovedUses() implementation was faulty and involved excessive computation. It was first noticed by the excessive lag in google sheets. & 2017.1.09 & 2017.3.14 & 18\\ \hline
		Bug 1250037 & The bug happens when box shadow increases beyond a certain height. Usually worse if the site uses animation.& 2016.2.21 & 2017.7.24 & 55                                                 \\ \hline
		Bug 1365333 & Slow performance in :nsLayoutUtils::PaintFrame function causes certain websites like "livenation" to lag. & 2017.5.16 & 2017.7.7 &   1                                                  \\ \hline
		Bug 1356605 & Poor performance in cookie storage causes Firefox to lag for web sites that have a large amount of cookie traffic & 2017.4.14 & 2017.6.27  & 6                                          \\ \hline
		Bug 1366203 & Visiting a slowly loading page and alter the url causes new url to load after a really long time in Firefox  & 2017.5.19 & 2017.7.05  &  77                                              \\ \hline
	\end{tabular}
	}
\end{table}

\subsubsection{Performance Bugs for Test Subject : VLC}
Our next test subject is the popular open source media player VLC. VLC uses its own bug reporting and tracking tool. VLC maintains its major repository in git \cite{git}. VLC was created in 2001 and also has a very extensive repository. However VLC does not mark performance bugs with any special category and hence mining performance bugs entails reading through the developer log for each bug. It took around $5$ months to find out all the bugs we have in our repository. We especially look for the bugs with any comments and descriptions with the keyword \textbf{slow}, \textbf{performance problem}. We have chosen bugs which have no platform dependency. Bugs for VLC fall into two categories, either it is a bug in the user interactions of the software or the bug manifests when a particular file is played. By user interactions we mean bugs that are caused by errors in a functionality of the software like playing a song or fast forwarding.
For VLC we have chosen the following bugs as seen in the table \ref{tab:VLCBugs}.

\begin{table}[h]
	\centering
	\caption{Bugs for VLC}
	\label{tab:VLCBugs}
	\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|l|l|l|l|l|l|}
		\hline
		Bug Number & Bug Description & Report Date & Resolution Date & Type & LOC affected                                                                       \\ \hline
		Bug 8821   & \multicolumn{1}{l|}{On trying to use "Capture Devices" feature causes VLC to hang} & Jun 20, 2013 &  Jul 8, 2016 & Interaction & 1   \\ \hline
		Bug 12944  & \multicolumn{1}{l|}{Vlc crashes trying to open files with VP9 handling} &Dec 4, 2014 & Feb 26, 2016 & File Type&2 \\ \hline
		Bug 13416  & \multicolumn{1}{l|}{Trying to stream an audio file over the internet cause VLC to hang.} &Dec 28, 2014&Jan 2, 2015&Interaction&1\\ \hline
		Bug 13727  & \multicolumn{1}{l|}{VLC crashes trying to open a .ts file}   &Jan 20, 2015&Apr 29, 2015&File Type&83 \\ \hline
		Bug 14046  & \multicolumn{1}{l|}{VLC hangs the computer after while playing a video}  &Mar 1, 2015&Apr 20, 2015& File type&2                \\ \hline
		Bug 14120  & \multicolumn{1}{l|}{VLC cannot decode avi file.}    &Jun 8, 2014&Apr 11, 2015&File Type&3 \\ \hline
	\end{tabular}
	}
\end{table}


\subsubsection{Performance Bugs for Kate}

Kate \cite{Kate} is an open source text editor created by KDE free software community. It has been a part of KDE software compilation since 2001. The text editor provides numerous development oriented features like syntax highlighting. Bugs for Kate are reported in KDE Bug tracking System \cite{kdebugtracker}. The main repository for Kate is Git. Kate does not specifically mark bugs with a specific performance tag like Firefox, on searching with words like \textit{performance}, \textit{slow} we could collect performance related bugs. It took around 2 months to extract the bugs. The bugs mined for Kate are in the table \ref{tab:kate}.

\begin{table}[h]
	\centering
	\caption{Bugs for Kate}
	\label{tab:kate}
	\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|l|l|l|l|l|}
		\hline
		Bug Number & Bug Description & Report Date & Resolution Date & LOC affected                                                               \\ \hline
		Bug 71099  & \multicolumn{1}{l|}{Kate is slow in repainting dynamically wrapped lines.}   &2016-01-23 &	2016-04-25&1 \\ \hline
		Bug 358526 & \multicolumn{1}{l|}{Kate hangs when changing the color scheme}&2016-01-25 &	2016-05-28 &1                         \\ \hline
		Bug 158200 & \multicolumn{1}{l|}{Deleting large amounts of text is slow for this version of Kate.}&	2008-02-22&2009-11-15&5 \\ \hline
		Bug 335952 & \multicolumn{1}{l|}{Changing indentation to c++/boost causes Kate to hang.} &2014-06-082&	2014-07-305&4         \\ \hline
		Bug 339338 & \multicolumn{1}{l|}{Kate is very slow to update for section highlighting} &2014-09-23 &2014-11-01&8 \\ \hline
		Bug 376504 & \multicolumn{1}{l|}{Switching input mode to vi causes Kate to crash}   &2017-02-15&2017-08-10&1              \\ \hline
	\end{tabular}
	}
\end{table}


\subsubsection{Bugs for LibreOffice}
Our last test subject is LibreOffice \cite{LibreOffice}. LibreOffice was forked from OpenOffice \cite{openoffice} on 2010. Bugzilla is used as the bug tracking and reporting tool for LibreOffice. LibreOffice uses Git to store its main repository. LibreOffice does not use a specific tag to indicate performance bug. The repository can be queried with  words like \textit{slow}, \textit{performance} to reveal performance bugs. It took around 2 weeks to gather the bugs for LibreOffice. Since a lot of new features are being implemented in the software that causes the nightly build to report performance issues. Table \ref{tab:libreoffice} outlines the performance bugs extracted from Libreoffice. 
\begin{table}
	\centering
	\caption{Bugs for Libreoffice}
	\label{tab:libreoffice}
	\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|l|p{13cm}| l|l|l|}
		\hline
		Bug Number & Bug Description  & Report Date & Reolution Date & LOC affected                                                                                                                    \\ \hline
		Bug 72413  & {Find and replace is slower for this version of LibreOffice}   &2013-12-06&2016-12-21&2                                                     \\ \hline
		Bug 8472   &{Libreoffice crashes on multiple uses of find/replace}&2014-10-07&2015-12-17&1                                                             \\ \hline
		Bug 77051  &{Opening a file with /f causes LibreOffice to perform poorly resulting in crash.}   &	2014-04-04&2015-01-24&1                                \\ \hline
		Bug 51733  & {Icons do not scale with high resolution screen and LibreOffice becomes slow to respond.} &2012-07-04&2017-08-22&87                          \\ \hline
		Bug 76979  & {Trying to save a document in word 2007/2010 cause Libreoffice to hang and corrupt the file.} &2014-04-03&2015-01-24 &6                     \\ \hline
		Bug 76930  & {Creating an Index causes libreoffice writer to fail.}  &	2014-04-02&2015-01-24& 1                                                           \\ \hline
		Bug 76122  & {Opening a word document containing word art causes LibreOffice to become unresponsive and also corrupts the file.}&2014-03-13&2015-01-24&18 \\ \hline
	\end{tabular}
	}
\end{table}

\subsubsection{Method for Experiments}
We extract the build before the patch for the bugs mentioned above were pushed in. We then run our approach on these builds. This allows us to ascertain whether our approach can actually find bugs. Most of these bug reports also have an extensive dependency bug reports which allows us to investigate other reported bugs to see if our approach is able to detect them.\par

Before we can proceed with our experiments we have to first decide the parameter for the approach, \textit{Mutation Rate} \cite{Mutation}. This parameter decides the rate at which a new random test case is generated to maintain the genetic diversity of the application. We also have to decide the seminal feature we intend to use in our approach. For this series of experiments we have used the following Seminal Features 

\begin{itemize}
	\item Loop counts - Count of the number of loops executed.
	\item Function counts - Count of the functions that were executed. 
	\item Sequence of functions - Sequence of the functions executed.
\end{itemize}
\begin{center}
\begin{figure}
\centering
	\begin{subfigure}[b]{0.60\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/NumVLC}
		\caption{Number of Test Cases for VLC}
		\label{fig:numvlc}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.60\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/NumFirefox}
		\caption{Number of test cases for Firefox}
		\label{fig:numfirefox}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.60\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/NumLibreOffice}
		\caption{Number of test cases for Libre Office}
		\label{fig:numlibre}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.60\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/NumKate}
		\caption{Number of test cases for Kate}
		\label{fig:NumKate}	
	\end{subfigure}
	\caption{The number of test cases created for each AUT}
	\label{fig:num}
\end{figure}
\end{center}
\begin{figure}[h]
	\includegraphics[width=1.0\linewidth]{images/Summary}
	\caption{Summary of the experiment for the \ref{fig:num}}
	\label{fig:summary}
\end{figure}

\subsection{Research Question 1: How effective is our approach in generating new test cases and detecting bugs?}
One of the major aims of this thesis was to create an approach that could create a test suite specifically targeting performance bugs. In the following experiment we find out that our approach can create new test cases and detect bugs based on a predecided mutation rate and a seminal feature. For this experiment we choose \textit{Sequence of function count} as the seminal feature. We execute our approach for all the bugs mined above for all our AUTs. We manually go through the generated tests to confirm whether the performance bug is of the same type as reported in the repository or it is a new performance bug unlike the reported one. 
We plot the results of the experiment in the following graph \ref{fig:num}. We plot for every bug in our repository the total number of test cases generated, the number of times the same bug was detected and the  total number of new bugs detected by the approach. We summarize our results for all the test subjects in the table \ref{fig:summary}.
\begin{center}
\begin{figure}
\centering
	\begin{subfigure}[b]{0.70\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/SeminalFeaturesVLC}
		\caption{Sensitivity with respect to the seminal feature for VLC}
		\label{fig:smvlc}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.70\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/SeminalFeaturesFirefox}
		\caption{Sensitivity with respect to the seminal feature for Firefox}
		\label{fig:smfirefox}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.70\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/SeminalFeaturesLibreOffice}
		\caption{Sensitivity with respect to the seminal feature for Libre Office}
		\label{fig:smlibre}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.70\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{images/SeminalFeaturesKate}
		\caption{Sensitivity with respect to the seminal feature for Kate}
		\label{fig:smkate}	
	\end{subfigure}
	
	\caption{The Effects of different Seminal Features on the AUTs}
	\label{fig:sm}
\end{figure}
\end{center}

\vspace{0.4cm}
\begin{tabular}{|p{0.88\textwidth}|}
	\hline
	Conclusion : Our approach identified $100\%$ of Bugs and generates a total of $1262$ test cases.\\
	\hline
\end{tabular}

\subsection{Research Question 2: What effect does Seminal feature have on our approach?}

Seminal feature is one of the two input parameters of our approach. A Seminal feature is responsible for the number of test cases created and also the number of bugs found. Our approach directly uses the correlation between the program features and the behaviour of the program. Different seminal features encode different level of information so a better information source should produce better results. For instance, sequence of function counts  \textit{Sequence of function counts} is a richer information source than \textit{Function Counts} as such the former should generate more performance stressing test inputs than the latter. We use the number of bugs revealed by the approach using that seminal feature as a criterion to evaluate the effectiveness of the seminal feature. For this experiments we have chosen three  Seminal Features : loop counts, Sequence of functions and function counts. We have executed our approach for all the bugs mined for all our AUTs. The results of the experiment is shown in figure \ref{fig:sm}.

We have plotted our results with each bug number on the x-axis and the number of bugs found on the y-axis for all the test subjects for all the seminal features shown above. This gives us a comparative view on the effect of seminal features. As we can see in the above graphs figure \ref{fig:sm}, seminal features have an impact on the number of bugs revealed. We can clearly see that \textit{Sequence of function counts} gets better results than the other seminal features. Since  \textit{Sequence of function counts} is a richer source of information than the other two seminal features, we can see the dependency between the information and the effect it has on generating test cases and revealing bugs.

\vspace{0.4cm}
\begin{tabular}{|p{0.88\textwidth}|}
	\hline
	Conclusion :
       Sequence of function counts produces more test cases and detects more bugs than any other seminal feature.\\
	\hline
\end{tabular}


% \begin{figure}
% 	\includegraphics[width=1.0\linewidth]{images/Random}
% 	\caption{The degree of non-determinism in our approach}
% 	\label{fig:num}
% \end{figure}


\subsection{Research Question 3: How do we account for non-determinism in our approach?}

\begin{figure}
    \centering
	\includegraphics[width=0.7\linewidth]{images/nondeter}
	\caption{The effect of non-determinism on our approach.}
	\label{fig:nondeter}
\end{figure}

Since our approach uses Neuro-evolution which uses genetic algorithm, there is a certain amount of non-determinism involved in the results of the experiments we get. Hence it becomes necessary to understand the effect of non-determinism on our approach or to what extent it influence the number of bugs found or the number of test cases created by the approach. In order to investigate this, we set up an experiment as follows, we execute our approach $10$ times for one test subject which we have chosen as Firefox and \textit{Sequence of function counts} as the input Seminal Feature.  \par
 We also summarize the result of the experiment in the figure \ref{fig:nondeter}. In the x-axis we plot the run number and in the y-axis we plot the average number of test cases produced by the approach and the average number of bugs found by the approach. While we can see that there is some non-determinism, there is not any significant changes in either number of bugs found or the number of test cases generated.

\vspace{0.4cm}
\begin{tabular}{|p{0.88\textwidth}|}
	\hline
       Conclusion : There is not much variation in the results when our approach is executed multiple times.\\
	\hline
\end{tabular}




\begin{figure}[h]
\centering
	\begin{subfigure}[b]{0.55\textwidth}
		\centering
		\includegraphics[width=1\linewidth]{images/BugsVsMuation}
		\caption{The Effect of Mutation rate on the Number of Bugs detected}
		\label{fig:muvsbg}	
	\end{subfigure}
	
	\begin{subfigure}[b]{0.55\textwidth}
		\centering
		\includegraphics[width=1\linewidth]{images/muationratevsnum}
		\caption{The Effect of Mutation rate on the Number of Test Cases Produced}
		\label{fig:muvstc}	
	\end{subfigure}
	\caption{The Effects of varying mutation rates}
	\label{fig:mut}
\end{figure}


\subsection{Research Question 4: What effect does mutation rate have on our approach?}

Mutation rate is an input parameter to our algorithm. It decides the variation in a particular generation and it is required to add variation to every generation and prevent any generation from stagnating. Varying mutation rate can have different impacts on the number of test case generated and also the number of bugs detected by our approach. We attempt to answer how mutation rate affects our approach by conducting an experiment. We execute our approach for all the bugs mined for AUT, Firefox for varying mutation rates from $0.01$ to $0.06$. The results of experiment is plotted in figure \ref{fig:mut}.

As we can see in the figure \ref{fig:muvstc} the number of test cases created increases with increased mutation rate. The number of bugs found also increases with an increase in mutation rate, however, this number stabilizes around $0.05$. 

It is interesting in the figure \ref{fig:muvsbg} to see that while the number of test cases found increases for mutation rate $0.06$ the number of bugs revealed decreases for the same. So we can conclude that while more test cases are being generated with an increase in mutation rate, the effectiveness of the test case with respect of revealing the performance bug decreases.

Hence we can conclude that for the AUT Firefox the approach performs the best with a mutation rate of $0.05$.

\vspace{0.4cm}
\begin{tabular}{|p{0.88\textwidth}|}
	\hline
      Conclusion : The results of our approach varies with the mutation rate. The number of test cases generated increases with an increase in mutation rate. The number of bugs detected increases and then plateaus for a particular mutation rate.\\
	\hline
\end{tabular}

\section{Threats to Validity}
Our approach is dependant on a few factors which may influence its behaviour and its ability to produce test cases and detect performance bugs. 

\begin{itemize}
    \item{Seminal Features} dictate how many tests are produced and how many bugs are revealed by the test hence seminal features are a crucial component of our proposed approach. This means choosing a seminal feature is of prime importance. Poor choice of seminal features will only have a slim chance of revealing performance bugs. 
    \item{Input Database} Our approach is highly dependant on the input database created for each AUT. The richness of the information contained in the database decides how variant test cases can be generated by our approach. A rich input database would mean more varied test cases , which, in turn will likely to reveal more performance bugs. The Input database can be complex depending on the AUT. For instance, mining information from web pages can be challenging as web pages are highly dynamic.
    \item{Mutation rate}: The only other parameter that is required by our approach is a mutation rate. This rate decides how many and how variant test cases can be generated. This rate can vary from AUT to AUT. Since this parameter is key to detect and reveal performance bugs figuring out the exact rate may require some trials during the experiment.
\end{itemize}

